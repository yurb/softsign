import 'regenerator-runtime/runtime';
import UI from './ui.js';
import State from './state.js';
import { loadSamples, startTransport } from './audio.js';

async function init () {
  const samples = await loadSamples();
  const state = new State(samples);
  const ui = new UI(state);
}

init();
