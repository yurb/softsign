import { random, shuffle } from 'lodash-es';
import Events from './msg-bus.js';
import { startAudio } from './audio.js';

const STEP_GAP = {'min': 3000, 'max': 15000};

function makePoints(total) {
  const segment = 2 * Math.PI / total;
  const points = Array(total).fill(undefined).map((_, idx) => {
    const angle = segment * idx;
    return {'x': 50 + 40 * Math.cos(angle),
            'y': 50 + 40 * Math.sin(angle)};
  });
  return points;
}

function* cycle(elements) {
  while (true) {
    for (let el of elements) {
      yield el;
    }
  }
}

export default class UI {
  constructor(state) {
    const container = document.getElementById("points");
    const loadingMsg = document.getElementById("loading");

    loadingMsg.classList.add("invisible");

    let screenPoints = makePoints(21);

    this.pointSeq = cycle(shuffle(screenPoints));
    this.state = state;
    this.container = container;

    this.step();

    Events.on('stop', (id) => {
      this.removeCheckBox(id);
    });

    Events.on('lock', (id) => {
      const checkbox = this.getCheckBox(id);
      checkbox.disabled = true;
    });

    Events.on('unlock', (id) => {
      const checkbox = this.getCheckBox(id);
      checkbox.disabled = false;
    });
  }

  step() {
    const node = this.state.getNext();
    const point = this.pointSeq.next().value;
    if (this.state.currentKey == "zero") {
      this.makeCheckBox(node, point);
    } else {
      setTimeout(
        () => { this.makeCheckBox(node, point); },
        window.godmode ? 0 : random(STEP_GAP.min, STEP_GAP.max)
      );
    }
  }

  getCheckBox(id) {
    return this.container.querySelector(`input[data-id="${id}"]`);
  }

  makeCheckBox(key, point) {
    let id = Math.random().toString(16).substring(2);
    let checkbox = document.createElement('input');
    checkbox.setAttribute("type", "checkbox");
    checkbox.classList.add("path-checkbox");
    checkbox.classList.add("next");
    checkbox.style.top = `${point.y}%`;
    checkbox.style.left = `${point.x}%`;
    checkbox.style.position = "absolute";
    checkbox.dataset.key = key;
    checkbox.dataset.id = id;
    checkbox.addEventListener("change", (evt) => { this.onChecked(evt.target) });
    this.container.append(checkbox);
  }

  removeCheckBox(key) {
    let checkbox = this.getCheckBox(key);
    checkbox.classList.add("invisible");
    setTimeout(() => { checkbox.remove() }, 3000);
  }

  onChecked(el) {
    const [key, id] = [el.dataset.key, el.dataset.id];
    if (this.state.currentKey == "zero")
      startAudio();
    if (el.checked) {
      if (el.classList.contains("next")) {
        el.classList.remove("next");
        this.state.step(key, id);
        this.step();
      } else {
        this.state.startSound(key, id);
      }
    } else {
      this.state.stopSound(id);
    }
  }
}
