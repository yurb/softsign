import 'regenerator-runtime/runtime';
import * as Tone from 'tone';

const SAMPLES_BASE = 'assets/lossy/';

function checkOggSupport() {
  const audio = document.createElement('audio');
  return (audio.canPlayType('audio/ogg; codecs=vorbis') == 'probably')
}

async function loadSample(url) {
  const ctx = Tone.getContext();
  const loaded = await fetch(url);
  const data = await loaded.arrayBuffer();
  const buf = await ctx.decodeAudioData(data);
  return buf;
}

class SampleLoader {
  constructor(base, supportsOgg) {
    this.base = base;
    this.supportsOgg = supportsOgg;
  }

  load(name) {
    const url = SAMPLES_BASE + name + (this.supportsOgg ? '.ogg' : '.aac');
    return loadSample(url);
  }
}

export async function loadSamples () {
  const loader = new SampleLoader(SAMPLES_BASE, checkOggSupport());

  return {
    'tones': [
      await loader.load('tones/ni/chords-01'),
      await loader.load('tones/ni/chords-02'),
    ],
    'tones-bass': [
      await loader.load('tones/sh/bass-tone-01'),
    ],
    'sighs': [
      await loader.load('sigh-tones/ni/sigh-layer-01'),
      await loader.load('sigh-tones/ni/sigh-layer-02'),
      await loader.load('sigh-tones/ni/sigh-layer-03'),
      await loader.load('sigh-tones/collective-01'),
    ],
    'screams': [
      await loader.load('screams/ni/screams-layer-01'),
      await loader.load('screams/ni/screams-layer-02'),
      await loader.load('screams/ni/screams-layer-03'),
    ],
    'consonants': [
      await loader.load('consonants/tk/chsh-01'),
      await loader.load('consonants/tk/kh-01'),
      await loader.load('consonants/tk/sh-01'),
      await loader.load('consonants/tk/sh-02'),

      await loader.load('consonants/ni/c-01'),
      await loader.load('consonants/ni/f-01'),
      await loader.load('consonants/ni/kh-01'),
      await loader.load('consonants/ni/kh-02'),
      await loader.load('consonants/ni/pa-01'),
      await loader.load('consonants/ni/pb-01'),
      await loader.load('consonants/ni/pb-02'),
      await loader.load('consonants/ni/r-01'),
      await loader.load('consonants/ni/r-02'),
      await loader.load('consonants/ni/r-03'),
      await loader.load('consonants/ni/s-01'),
      await loader.load('consonants/ni/t-01'),
      await loader.load('consonants/ni/t-02'),
    ],

    'laugh-cough': [
      await loader.load('laugh-cough/ni/hm-laugh-01'),
      await loader.load('laugh-cough/ni/laugh-01'),
      await loader.load('laugh-cough/ni/laugh-02'),
      await loader.load('laugh-cough/tk/cough-01'),
      await loader.load('laugh-cough/tk/cough-02'),
      await loader.load('laugh-cough/sh/laugh-01'),
      await loader.load('laugh-cough/sh/laugh-02'),
    ],
  }
}

function makeMaster() {
  Tone.setContext(new Tone.Context({latencyHint: "playback"}));
  const reverb = new Tone.Convolver('assets/ir.wav').toDestination();
  const delayL = new Tone.FeedbackDelay(0.5, 0.7);
  const delayR = new Tone.FeedbackDelay(0.63, 0.7);
  const delayM = new Tone.FeedbackDelay(0.15, 0.34);
  const delaySplit = new Tone.Split();
  const delayMerge = new Tone.Merge();
  const master = new Tone.Gain(0.5);
  const dry = new Tone.Gain(1).toDestination();

  master.connect(delaySplit);
  master.connect(delayM);
  delayM.connect(reverb);
  delaySplit.connect(delayL, 0, 0);
  delaySplit.connect(delayR, 1, 0);
  delayL.connect(delayMerge, 0, 0);
  delayR.connect(delayMerge, 0, 1);

  delayMerge.connect(reverb);
  master.connect(dry);
  master.connect(reverb);
  return master;
}

export async function startAudio() {
  await Tone.start();
  Tone.getTransport().start();
}

export const master = makeMaster();
