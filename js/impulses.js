import 'regenerator-runtime/runtime';
import { random, sample as choose } from 'lodash-es';
import * as Tone from 'tone';

import { master } from './audio.js';

export default class Impulses {
  constructor({samples, rate, range,
               reverseChance, fragments = false, fragmentDur = [0.1, 0.4],
               filFreq = [300, 3000], amp = 0.5}) {
    this.samples = samples;
    this.fadeTime = 4;
    this.rate = rate;
    this.range = range;
    this.reverseChance = reverseChance;
    this.fragments = fragments;
    this.fragmentDur = fragmentDur;
    this.filFreq = random(filFreq[0], filFreq[1]);
    this.amp = amp;
  }

  makePlayer(sample) {
    const player = new Tone.Player(sample);
    player.connect(this.distortion);
    return player;
  }

  scheduleEvent(curTime) {
    if (this.playing) {
      const next = "+" + random(this.range, true);
      Tone.getTransport().scheduleOnce((time) => { this.event(time) }, next);
    } else {
      setTimeout(() => { this._dispose() }, this.fadeTime * 1000);
    }
  }

  event(time) {
    const player = this.makePlayer(choose(this.samples));
    const reverse = Math.random() < this.reverseChance;
    const offset = this.fragments ? random(player.buffer.duration * 0.6, true) : 0;
    const duration = this.fragments ? random(this.fragmentDur[0], this.fragmentDur[1], true) : undefined;
    player.playbackRate = this.rate + (Math.random() * 0.1);
    player.reverse = reverse;
    player.start(time, offset, duration);
    this.scheduleEvent(time);
  }

  start() {
    if (!this.playing) {
      this.gain = new Tone.Gain(0.5);
      this.gain.connect(master);

      this.distortion = new Tone.BiquadFilter(this.filFreq, "bandpass");
      this.distortion.connect(this.gain);

      this.playing = true;
      this.scheduleEvent(Tone.now());
    } else {
      console.log("impulses: clash!");
    }
  }

  stopAndDispose() {
    this.playing = false;
    return new Promise((resolve, reject) => {
      setTimeout(resolve, this.fadeTime * 1000);
    });
  }

  _dispose() {
    this.distortion.dispose();
    this.gain.dispose();
  }
}
