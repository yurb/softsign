import { shuffle, sample as choose } from 'lodash-es';
import { ChoirTone } from './tones.js';
import Impulses from './impulses.js';
import Events from './msg-bus.js';

const TRAIL_LENGTH = 5;

function wrandom (node) {
  const weights = Object.keys(node);
  const probabilities = weights.map(w => Math.random() * w);
  const highest = Math.max(...probabilities);
  const winnerKey = weights[probabilities.indexOf(highest)];
  return node[winnerKey];
}

export default class State {
  constructor(samples) {
    this._samples = samples;
    this.active = {};
    this.sounds = {
      "choir-tone-01": () => new ChoirTone(samples.tones),
      "choir-tone-02": () => new ChoirTone(samples.tones),
      "choir-tone-03": () => new ChoirTone(samples["tones-bass"]),
      "choir-tone-04": () => new ChoirTone(samples.tones, Math.random() * 0.2),
      "choir-tone-05": () => new ChoirTone(samples["tones-bass"]),

      "distort-tone-01": () => new ChoirTone(samples.tones, 1),
      "distort-tone-02": () => new ChoirTone(samples.tones, 1),
      "distort-tone-03": () => new ChoirTone(samples.tones, 1),

      "sigh-tone-01": () => new ChoirTone(samples.sighs),
      "sigh-tone-02": () => new ChoirTone(samples.sighs, 0.3),
      "sigh-tone-03": () => new ChoirTone(samples.sighs),

      "consonants-01": () => new Impulses({
        samples: samples.consonants,
        rate: 0.8,
        range: 5,
        reverseChance: 0.4
      }),
      "consonants-02": () => new Impulses({
        samples: samples.consonants,
        rate: 0.7,
        range: 10,
        reverseChance: 0.7
      }),

      "scream-01": () => new ChoirTone(samples.screams),
      "scream-02": () => new ChoirTone(samples.screams),
      "scream-03": () => new ChoirTone(samples.screams, 0.2),

      "laugh-cough-01": () => new Impulses({
        samples: samples["laugh-cough"],
        rate: 1,
        range: 1,
        reverseChance: 0.2,
        fragments: true,
        fragmentDur: [0.1, 0.4],
        filFreq: [4000, 7000],
        amp: 0.3,
      }),
      "laugh-cough-02": () => new Impulses({
        samples: samples["laugh-cough"],
        rate: 1,
        range: 0.2,
        reverseChance: 0.5,
        fragments: true,
        fragmentDur: [0.05, 0.1],
        amp: 0.5,
      }),
      "laugh-cough-03": () => new Impulses({
        samples: samples["laugh-cough"],
        rate: 1,
        range: 0.05,
        reverseChance: 0.5,
        fragments: true,
        fragmentDur: [0.02, 0.04],
        filFreq: [3000, 7000],
        amp: 0.7,
      }),
      "laugh-cough-04": () => new Impulses({
        samples: samples["laugh-cough"],
        rate: 1,
        range: 0.5,
        reverseChance: 0.5,
        fragments: true,
        fragmentDur: [0.02, 0.1],
        filFreq: [8000, 9000],
        amp: 0.2,
      }),
    };
    this.currentKey = "zero";
    this.trail = [];
    this.chain = {
      "zero": {
        1: [
          "consonants-01",
        ]
      },
      "consonants-01": {
        1: [
          "sigh-tone-01",
        ]
      },
      "consonants-02": {
        1: [
          "choir-tone-03",
        ]
      },
      "choir-tone-01": {
        1: [
          "choir-tone-02",
        ]
      },
      "choir-tone-02": {
        2: [
          "choir-tone-03",
        ],
        1: [
          "distort-tone-01",
        ],
      },
      "choir-tone-03": {
        1: [
          "choir-tone-04",
          "sigh-tone-01",
        ]
      },
      "choir-tone-04": {
        1: [
          "choir-tone-05",
        ],
        0.3: [
          "laugh-cough-01",
        ],
      },
      "choir-tone-05": {
        1: [
          "sigh-tone-01",
        ],
        2: [
          "scream-01",
          "scream-03",
        ],
      },
      "sigh-tone-01": {
        1: [
          "sigh-tone-02",
          "choir-tone-01",
        ]
      },
      "sigh-tone-02": {
        1: [
          "sigh-tone-03",
        ]
      },
      "sigh-tone-03": {
        1: [
          "choir-tone-01",
          "distort-tone-01",
          "scream-01",
        ],
      },
      "scream-01": {
        0.2: [
          "consonants-02",
        ],
        1: [
          "scream-02",
          "distort-tone-02",
        ]
      },
      "scream-02": {
        2: [
          "choir-tone-04",
          "distort-tone-01",
        ],
        0.2: [
          "consonants-01",
        ]
      },
      "scream-03": {
        2: [
          "choir-tone-04",
        ],
        1: [
          "laugh-cough-02",
          "distort-tone-01",
        ]
      },
      "distort-tone-01": {
        2: [
          "distort-tone-02",
        ],
        1: [
          "choir-tone-04",
        ]
      },
      "distort-tone-02": {
        3: [
          "distort-tone-03",
        ],
        2: [
          "scream-03",
        ],
        1: [
          "consonants-01",
        ]
      },
      "distort-tone-03": {
        2: [
          "choir-tone-02",
        ],
        1: [
          "distort-tone-01",
          "laugh-cough-04",
        ],
      },
      "laugh-cough-01": {
        2: [
          "laugh-cough-02",
          "choir-tone-02",
          "consonants-02",
        ],
        1: [
          "distort-tone-01",
        ],
      },
      "laugh-cough-02": {
        2: [
          "laugh-cough-03",
        ],
        1: [
          "distort-tone-01",
        ],
      },
      "laugh-cough-03": {
        2: [
          "choir-tone-01",
        ],
        1: [
          "distort-tone-01",
        ],
      },
      "laugh-cough-04": {
        2: [
          "consonants-02",
        ],
        1: [
          "sigh-tone-01",
        ],
      },
    }
  }

  getNext() {
    const curNode = this.chain[this.currentKey];
    const choice = choose(wrandom(curNode));
    return choice;
  }

  step(key, id) {
    console.log("Current node: ", key);
    console.log("Next possibilities: ", this.chain[key]);
    this.currentKey = key;
    this.trail.push(id);
    this.startSound(key, id);

    if (this.trail.length > TRAIL_LENGTH) {
      const toStop = this.trail.shift();
      Events.emit('stop', toStop);
      this.active[toStop].stopAndDispose();
      delete this.active[toStop];
    }
  }

  startSound(key, id) {
    if (this.active[id]) {
      this.active[id].start();
    } else {
      const instance = new this.sounds[key];
      instance.start();
      this.active[id] = instance;
    }
  }

  stopSound(id) {
    Events.emit('lock', id);
    this.active[id].stopAndDispose().then(() => {
      Events.emit('unlock', id);
    });
  }
}
