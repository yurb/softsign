import 'regenerator-runtime/runtime';
import { sample as choose } from 'lodash-es';
import * as Tone from 'tone';

import { master } from './audio.js';

function SampleTone(note, sample, fadeTime, distortGain = 0, dryGain = 1) {
  const voice = new Tone.Player(sample);
  const gainMod = new Tone.LFO(0.01 + (Math.random() * 0.05), 0.2, 0.5);
  const panMod = new Tone.LFO(0.02 + (Math.random() * 0.05), -0.5, 0.5);
  const gain = new Tone.Gain(1);
  const panner = new Tone.Panner();
  const distort = new Tone.Chebyshev(55);
  const distortG = new Tone.Gain(distortGain);

  gainMod.phase = Math.random() * 360;
  panMod.phase = Math.random() * 360;

  gainMod.start();
  panMod.start();

  voice.fadeIn = 7;
  voice.fadeOut = fadeTime;
  voice.autostart = true;
  voice.loop = true;
  voice.playbackRate = note;

  gainMod.connect(gain.gain);
  panMod.connect(panner.pan);

  voice.connect(panner);
  panner.connect(gain);
  panner.connect(distort);
  distort.connect(distortG);
  distortG.connect(gain);
  gain.connect(master);
  voice.start();

  this.fadeTime = voice.fadeOut;
  this.mainNode = voice;
  this.nodes = [voice, panMod, gainMod, panner, distort, distortG, gain];
}

export class ChoirTone {
  constructor (samples, distortion = 0) {
    this.chord = [
      1, 1.31, 1.52, 1.58, 1.71,
    ].map(item => item * 0.8);
    this.sound = null;
    this.fadeTime = 1;
    this.distortion = distortion;
    this.samples = samples;
  }

  start() {
    if (!this.sound)
      this.sound = new SampleTone(choose(this.chord), choose(this.samples), this.fadeTime, this.distortion);
    else console.log("clash!");
  }

  stopAndDispose() {
    return new Promise((resolve, reject) => {
      this.sound.mainNode.stop();
      setTimeout(() => {
        this.sound.mainNode.dispose();
        this.sound.nodes.forEach((node) => { if (!node.disposed) node.dispose() });
        this.sound = null;
        resolve();
      }, this.fadeTime * 1500);
    });
  }
}
